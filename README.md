<h3 align="center">
  greenplat_challenge 🍂
</h3>

<p align="center">
  The "greenplat_challenge" is a app to register users.
</p>

### Deployed in Vercel

[Greenplat Challenge](https://greenplat-challenge.vercel.app/)

### 📦 Content

1. [Run the project](#-run-the-project)
2. [Tech stack](#-tech-stack)
3. [To do](#-to-do)

### 🏁 Run the project

**Docker**

```shell
docker-compose up -d --build
```

**Node JS (with yarn)**

Run app

```shell
yarn start
```

Create a build

```shell
yarn build
```

Run lint

```shell
yarn lint
```

### 🧩 Tech stack

- [React JS](reactjs.org)
- [Typescript](https://www.typescriptlang.org/)
- [Docker](https://docs.docker.com/)
- [Styled Components](https://styled-components.com/)
- [React Table](https://react-table.tanstack.com/)
- [Vercel](https://vercel.com)

### 📝 To do

- Add toasts
