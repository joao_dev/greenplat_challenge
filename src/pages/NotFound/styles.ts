import styled from 'styled-components';

export const Container = styled.div`
  min-height: 100vh;
  padding: 32px;
`;

export const Title = styled.div`
  color: ${({ theme }) => theme.gray6};

  font-size: 34px;
  font-weight: 800;
`;
