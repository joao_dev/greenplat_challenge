import { Container, Title } from './styles';

const NotFound = (): JSX.Element => {
  return (
    <Container>
      <Title>NotFound</Title>
    </Container>
  );
};

export default NotFound;
