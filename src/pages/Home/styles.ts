import styled from 'styled-components';

export const Container = styled.div`
  padding: 32px;
`;

export const Content = styled.div`
  max-width: 1276px;
  width: 100%;
  margin: 0 auto;

  form {
    margin-bottom: 20px;
  }
`;

export const FormContainer = styled.div`
  padding: 15px 20px 20px 20px;
  background-color: #f5f5f5;
  box-shadow: 0px 2px 2px #00000067;
  border-radius: 4px;
`;

export const Head = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;

  padding-bottom: 10px;
  margin-bottom: 24px;

  border-bottom: 1px solid #cccccc;
`;

export const HeadTitle = styled.div`
  display: flex;
  align-items: center;

  color: ${({ theme }) => theme.gray3};
  font-size: 28px;
`;

export const HeadIcon = styled.img`
  margin-right: 10px;
`;

export const HelperText = styled.div`
  color: ${({ theme }) => theme.gray4};
  font-size: 18px;
`;
