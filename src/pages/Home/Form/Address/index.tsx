import {
  ForwardRefRenderFunction,
  forwardRef,
  useCallback,
  useContext,
  useImperativeHandle,
  useRef,
} from 'react';

import axios from 'axios';
import Select from 'src/components/Select';
import { UserContext } from 'src/contexts/Users';

export interface IAddressHandlers {
  setValue(value: any): void;
  clearValue(): void;
}

interface ISelectState {
  state: {
    value?: {
      label?: string;
    };
  };
  select: {
    setValue(value: any): void;
  };
}

const Address: ForwardRefRenderFunction<IAddressHandlers> = (
  props,
  ref
): JSX.Element => {
  const context = useContext(UserContext);

  const cityRef = useRef<ISelectState>(null);
  const stateRef = useRef<ISelectState>(null);

  useImperativeHandle(
    ref,
    () => ({
      setValue: ({ state, city }) => {
        if (stateRef.current?.select && context?.states) {
          const stateFind = context.states.find(
            (item) => item.value.toString() === state
          );
          stateRef.current.select.setValue(stateFind || {});
        }

        if (cityRef.current?.select && context?.cities) {
          const cityFind = context.cities.find(
            (item) => item.value.toString() === city
          );
          cityRef.current.select.setValue(cityFind || {});
        }

        return;
      },
      clearValue: () => {
        cityRef.current?.select?.setValue({});
        stateRef.current?.select?.setValue({});
      },
    }),
    [context]
  );

  const handleOnChangeState = useCallback(
    ({ value }) => {
      context?.changeState(value);
      cityRef.current?.select?.setValue({});
    },
    [context]
  );

  return (
    <>
      <Select
        label="Estado"
        name="state"
        options={context?.states || []}
        onChange={handleOnChangeState}
        selectRef={stateRef}
        required
      />
      <Select
        label="Cidade"
        name="city"
        options={context?.citiesOptions || []}
        selectRef={cityRef}
        required
      />
    </>
  );
};

Address.displayName = 'Address';

export default forwardRef(Address);
