import { useContext, useEffect, useRef } from 'react';

import Button, { IButtonHandlers } from 'src/components/Button';
import FormGroup from 'src/components/FormGroup';
import Input from 'src/components/Input';
import { UserContext } from 'src/contexts/Users';
import getData from 'src/utils/getData';
import setData from 'src/utils/setData';
import { v4 as uuidv4 } from 'uuid';

import Address, { IAddressHandlers } from './Address';
import { Container, IconButton } from './styles';

const Form = (): JSX.Element => {
  const context = useContext(UserContext);

  const addressRef = useRef<IAddressHandlers>(null);
  const buttonRef = useRef<IButtonHandlers>(null);

  // When user click in edit, set data in form
  useEffect(() => {
    if (context?.userToEdit) {
      addressRef.current?.setValue(context.userToEdit);
      setData('user_form', context.userToEdit);
    }
  }, [context?.userToEdit]);

  // Returns the current date in form "yyyy-mm-dd"
  const getCurrentDate = (): string => {
    const currentDate = new Date()
      .toLocaleDateString('pt-BR', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
      })
      .split('/')
      .reverse()
      .join('-');

    return currentDate;
  };

  const handleOnSubmit = (event) => {
    event.preventDefault();

    buttonRef.current?.startLoad();

    // Get data from form elements
    const { full_name, cpf, born_date, state, city } = getData(
      event.target.elements
    );

    // Simulate call api
    setTimeout(() => {
      const userData = {
        full_name,
        cpf,
        born_date,
        state,
        city,
      };

      // If has content in userToEdit update exist user
      if (!context?.userToEdit) {
        context?.addUser({
          id: uuidv4(),
          ...userData,
        });
      } else {
        context?.updateUser({
          id: context?.userToEdit.id,
          ...userData,
        });
      }

      event.target.reset();
      addressRef.current?.clearValue();
      buttonRef.current?.finishLoad();
    }, 200);
  };

  const handleOnClickCancel = () => {
    context?.cancelEdit();
    addressRef.current?.clearValue();
    document?.forms['user_form']?.reset();
  };

  return (
    <Container id="user_form" onSubmit={handleOnSubmit}>
      <FormGroup columns="1fr" gap={20}>
        <FormGroup columns="2fr 1fr 1fr" gap={20}>
          <Input label="Nome Completo" name="full_name" required />
          <Input label="CPF" name="cpf" mask="cpf" required />
          <Input
            label="Data de nascimento"
            name="born_date"
            type="date"
            min="1900-01-01"
            max={getCurrentDate()}
            required
          />
        </FormGroup>
        <FormGroup
          columns={
            context?.userToEdit ? '2fr 2fr 170px 170px' : '2fr 2fr 1.2fr'
          }
          gap={20}
        >
          <Address ref={addressRef} />
          {context?.userToEdit && (
            <Button
              icon={<IconButton src="/assets/cancel.svg" />}
              onClick={handleOnClickCancel}
              label="Cancelar"
              variant="cancel"
            />
          )}
          <Button
            ref={buttonRef}
            icon={<IconButton src="/assets/arrow-down.svg" />}
            label={context?.userToEdit ? 'Salvar' : 'Incluir'}
            type="submit"
          />
        </FormGroup>
      </FormGroup>
    </Container>
  );
};

export default Form;
