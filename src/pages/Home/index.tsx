import { useCallback, useEffect, useState } from 'react';

import axios from 'axios';
import {
  ICities,
  IStates,
  IUser,
  UserContextProvider,
} from 'src/contexts/Users';

import Form from './Form';
import List from './List';
import {
  Container,
  Content,
  FormContainer,
  Head,
  HeadIcon,
  HeadTitle,
  HelperText,
} from './styles';

const Home = (): JSX.Element => {
  const [userToEdit, setUserToEdit] = useState<IUser>();
  const [users, setUsers] = useState<IUser[]>([]);
  const [cities, setCities] = useState<ICities[]>([]);
  const [citiesOptions, setCitiesOptions] = useState<ICities[]>([]);
  const [states, setStates] = useState<IStates[]>([]);

  useEffect(() => {
    const getStates = async () => {
      try {
        const response = await axios.get(
          'https://servicodados.ibge.gov.br/api/v1/localidades/estados'
        );

        const options = response.data.map(({ nome, id }) => ({
          label: nome,
          value: id,
        }));

        setStates(options);
      } catch (error) {
        console.log(error);
      }
    };

    getStates();
  }, []);

  useEffect(() => {
    const getCities = async () => {
      try {
        const response = await axios.get(
          'https://servicodados.ibge.gov.br/api/v1/localidades/municipios'
        );

        const options = response.data.map(({ id, nome, microrregiao }) => ({
          label: nome,
          value: id,
          stateId:
            microrregiao.mesorregiao.UF.id ||
            microrregiao['regiao-intermediara'].UF.id,
        }));

        setCities(options);
      } catch (error) {
        console.log(error);
      }
    };

    getCities();
  }, [states]);

  // Value is UF id
  const changeState = useCallback(
    async (value) => {
      try {
        const options = cities.filter((city) => city.stateId === value);

        setCitiesOptions(options);
      } catch (error) {
        console.log(error);
      }
    },
    [cities]
  );

  const value = {
    users,
    userToEdit,
    states,
    cities,
    citiesOptions,
    changeState,
    cancelEdit: () => setUserToEdit(undefined),
    addUser: (newUser) => {
      setUsers((currentUsers) => {
        const updatedUsers = [newUser, ...currentUsers];

        return updatedUsers;
      });
    },
    deleteUser: (id) => {
      setUsers((currentUsers) => {
        const updatedUsers = currentUsers.filter(
          (user: IUser) => user.id !== id
        );

        return updatedUsers;
      });
    },
    editUser: (user) => {
      setUserToEdit(user);
    },
    updateUser: (updatedUser) => {
      setUsers((currentUsers) => {
        const oldRemoved = currentUsers.filter(
          (user: IUser) => user.id !== updatedUser.id
        );

        const updatedUsers = [updatedUser, ...oldRemoved];

        return updatedUsers;
      });

      setUserToEdit(undefined);
    },
  };

  return (
    <Container>
      <Content>
        <Head>
          <HeadTitle>
            <HeadIcon loading="lazy" src="/assets/person.svg" />
            Cadastro de usuário
          </HeadTitle>
          <HelperText>*campos obrigatórios</HelperText>
        </Head>
        <FormContainer>
          <UserContextProvider value={value}>
            <Form />
            <List />
          </UserContextProvider>
        </FormContainer>
      </Content>
    </Container>
  );
};

export default Home;
