import { useContext } from 'react';

import axios from 'axios';
import { Column } from 'react-table';
import Table from 'src/components/Table';
import ActionButton from 'src/components/Table/ActionButton';
import { UserContext } from 'src/contexts/Users';

const List = (): JSX.Element => {
  const context = useContext(UserContext);

  const handleOnClickEdit = (data) => {
    context?.editUser(data);
  };

  const handleOnClickDelete = (event) => {
    const id = event.target.value;
    context?.deleteUser(id);
  };

  const calcAge = (bornDate) => {
    const ageDiffMs = new Date().getTime() - new Date(bornDate).getTime();
    const ageDate = new Date(ageDiffMs);

    return Math.abs(ageDate.getUTCFullYear() - 1970);
  };

  const formatDate = (date) => {
    return new Date(date).toLocaleDateString('pt-BR');
  };

  const getCityName = (cityId) => {
    const city = context?.cities.find(
      ({ value }) => value.toString() === cityId
    );

    return city?.label || '-';
  };

  const columns: Column[] = [
    {
      Header: 'Nome',
      accessor: 'full_name',
    },
    {
      Header: 'CPF',
      accessor: 'cpf',
    },
    {
      Header: 'Data de nascimento',
      accessor: ({ born_date }) => formatDate(born_date),
    },
    {
      Header: 'Idade',
      accessor: ({ born_date }) => {
        return calcAge(born_date);
      },
    },
    {
      Header: 'Estado',
      accessor: ({ state }) => {
        const stateFind = context?.states.find(
          (item) => item.value.toString() === state
        );

        return stateFind?.label || '-';
      },
    },
    {
      Header: 'Cidade',
      accessor: ({ city }) => getCityName(city),
    },
    {
      Header: 'Editar',
      // eslint-disable-next-line react/display-name
      accessor: (data) => {
        return (
          <ActionButton
            tooltip={
              context?.userToEdit?.id === data.id
                ? 'Usuário em edição'
                : 'Editar'
            }
            iconPath="/assets/edit.svg"
            disabled={context?.userToEdit?.id === data.id}
            onClick={() => handleOnClickEdit(data)}
          />
        );
      },
    },
    {
      Header: 'Excluir',
      // eslint-disable-next-line react/display-name
      accessor: ({ id }) => {
        return (
          <ActionButton
            tooltip={
              context?.userToEdit?.id === id ? 'Usuário em edição' : 'Deletar'
            }
            iconPath="/assets/trash.svg"
            value={id}
            disabled={context?.userToEdit?.id === id}
            onClick={handleOnClickDelete}
          />
        );
      },
    },
  ];

  return (
    <Table
      columns={columns}
      data={context?.users || []}
      loading={false}
      onChangePage={() => null}
      totalData={context?.users.length || 0}
    />
  );
};

export default List;
