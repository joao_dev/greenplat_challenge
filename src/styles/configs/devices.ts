const mediaQuery = (width: number, type = 'min'): string =>
  `@media (${type}-width: ${width}px)`;

const sizes = {
  desktop: 1024,
  tablet: 768,
  mobile: 520,
};

const media = {
  custom: mediaQuery,
  desktop: mediaQuery(sizes.desktop),
  tablet: mediaQuery(sizes.tablet),
  mobile: mediaQuery(sizes.mobile),
  max: {
    desktop: mediaQuery(sizes.desktop, 'max'),
    tablet: mediaQuery(sizes.tablet, 'max'),
    mobile: mediaQuery(sizes.mobile, 'max'),
  },
};

export default media;
