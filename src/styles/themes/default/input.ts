import { colors } from './_colors';

export default {
  color: colors.gray5,
  backgroundColor: '#FFFFFF',
  borderColor: '#D0D0D0',
};
