export const primary = {
  light: '#65BF24',
  normal: '#5CAC21',
};

export const colors = {
  gray1: '#515151',
  gray2: '#B0B0B0',
  gray3: '#515151',
  gray4: '#303030',
  gray5: '#202020',
  gray6: '#101010',
  error: '#FF3B30',
  info: '#5AC8FA',
  warning: '#FFCC01',
  primary,
  success: '#5CAC21',
};

export default {
  ...colors,
  backgroundColor: '#ffffff',
};
