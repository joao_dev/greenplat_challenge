import { colors, primary } from './_colors';

export default {
  primary: {
    color: '#FFFFFF',
    backgroundColor: primary.normal,
    borderColor: primary.normal,
    pulseLoader: {
      color: '#FFFFFF',
    },
    hover: {
      color: '#FFFFFF',
      borderColor: primary.light,
      backgroundColor: primary.light,
    },
  },
  cancel: {
    color: colors.gray3,
    borderColor: '#bdbdbd',
    backgroundColor: 'transparent',
    pulseLoader: {
      color: '#FFFFFF',
    },
    hover: {
      color: colors.gray4,
      borderColor: '#888888',
      backgroundColor: 'transparent',
    },
  },
};
