import { primary } from './_colors';

export default {
  borderColor: '#D3D3D3',
  backgroundColorHeader: '#E0E0E0',
  backgroundColorBody: '#ffffff',
  viewButton: {
    normal: { foreground: '#ffffff', background: primary.normal },
    hover: { foreground: '#ffffff', background: primary.light },
  },
};
