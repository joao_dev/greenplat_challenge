import colors from './_colors';
import button from './button';
import header from './header';
import input from './input';
import pulseLoader from './pulseLoader';
import table from './table';

const defaultTheme = {
  ...colors,
  button,
  input,
  header,
  pulseLoader,
  table,
};

type TTheme = typeof defaultTheme;
const theme: TTheme = defaultTheme;

export default theme;
