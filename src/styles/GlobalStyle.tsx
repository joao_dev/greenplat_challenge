import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
   box-sizing: border-box;
   padding: 0;
   margin: 0;
    outline: 0;
  }

  body {
    -webkit-font-smoothing: antialiased;
    font-family: 'Roboto', sans-serif;
    background-color: #ffffff;

    input,
    textarea,
    button {
      font-family: 'Roboto', sans-serif;
    }

    button {
      cursor: pointer;
    }
  }
`;

export default GlobalStyle;
