interface IGetData {
  [field: string]: any;
}

const getData = (elements: any): IGetData => {
  const values = {};

  Object.keys(elements).forEach((element) => {
    if (!Number(element)) {
      const { name, value } = elements[element];

      values[name] = value;
    }
  });
  return values;
};

export default getData;
