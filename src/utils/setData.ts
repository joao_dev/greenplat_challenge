interface IData {
  [field: string]: any;
}

const setData = (formId: string, data: IData): void => {
  if (!document.forms[formId]) {
    return;
  }

  const elements = document.forms[formId].elements;

  Object.keys(elements).forEach((key) => {
    if (!Number(key)) {
      elements[key].value = data[key] || '';
    }
  });
};

export default setData;
