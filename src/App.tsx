import { ThemeProvider } from 'styled-components';

import { createBrowserHistory } from 'history';
import Helmet from 'react-helmet';
import { Router } from 'react-router-dom';
import Routes from 'src/routes/Routes';
import GlobalStyle from 'src/styles/GlobalStyle';
import defaultTheme from 'src/styles/themes/default';

const history = createBrowserHistory();
const App = (): JSX.Element => {
  return (
    <>
      <Helmet>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap"
        />
      </Helmet>
      <Router history={history}>
        <ThemeProvider theme={defaultTheme}>
          <GlobalStyle />
          <Routes />
        </ThemeProvider>
      </Router>
    </>
  );
};

export default App;
