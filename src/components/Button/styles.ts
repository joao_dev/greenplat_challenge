import styled, { css } from 'styled-components';

const getButtonStyle = (variant, theme) => {
  const { backgroundColor, borderColor, color, hover } = theme.button[variant];

  return css`
    color: ${color};
    border-color: ${borderColor};
    background-color: ${backgroundColor};

    &:hover {
      color: ${hover.color};
      border-color: ${hover.borderColor};
      background-color: ${hover.backgroundColor};
    }
  `;
};

interface IContainerProps {
  variant: string;
}

export const Container = styled.button<IContainerProps>`
  display: flex;
  justify-content: center;
  align-items: center;

  border: 1px solid transparent;
  padding: 8px 48px;
  height: 38px;

  ${({ theme, variant }) => getButtonStyle(variant, theme)}

  text-transform: uppercase;
  font-size: 16px;
  font-weight: 600;

  transition: background-color 200ms linear, border-color 200ms linear,
    color 200ms linear;

  border-radius: 4px;

  * {
    margin-right: 12px;
  }
`;
