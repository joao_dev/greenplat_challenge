import {
  ComponentType,
  ForwardRefRenderFunction,
  forwardRef,
  ButtonHTMLAttributes,
  useContext,
  useImperativeHandle,
  useState,
  cloneElement,
} from 'react';

import { ThemeContext } from 'styled-components';

import PulseLoader from 'src/components/PulseLoader';

import { Container } from './styles';

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  icon?: JSX.Element;
  label: string;
  variant?: 'primary' | 'cancel';
}

export interface IButtonHandlers {
  finishLoad(): void;
  startLoad(): void;
}

const Button: ForwardRefRenderFunction<IButtonHandlers, IButtonProps> = (
  { icon, label, onClick, type = 'button', variant = 'primary' },
  ref
): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const themeContext = useContext(ThemeContext);

  // Allow imperative control the load of button
  useImperativeHandle(ref, () => ({
    finishLoad: () => setLoading(false),
    startLoad: () => setLoading(true),
  }));

  return (
    <Container
      type={type}
      disabled={loading}
      onClick={onClick}
      variant={variant}
      className="root-button"
    >
      {loading ? (
        <PulseLoader color={themeContext.button[variant].pulseLoader.color} />
      ) : (
        <>
          {icon && cloneElement(icon)}
          {label}
        </>
      )}
    </Container>
  );
};

Button.displayName = 'Button';

export default forwardRef(Button);
