import styled from 'styled-components';

export const Container = styled.label`
  display: inline-block;

  color: ${({ theme }) => theme.gray6};
  font-size: 16px;
  font-weight: 400;
`;
