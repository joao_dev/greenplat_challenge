import styled from 'styled-components';

interface IContainerProps {
  tooltip: string;
}

export const Container = styled.button<IContainerProps>`
  position: relative;
  width: 100%;

  display: flex;
  align-items: center;
  justify-content: center;

  height: 24px;
  width: 24px;

  border-radius: 4px;

  background-color: transparent;
  border: none;

  * {
    pointer-events: none;
  }

  &:after {
    content: ${({ tooltip }) => `'${tooltip}'`};
    position: absolute;
    bottom: calc(100% + 4px);
    left: 50%;
    transform: translateX(-50%);

    border-radius: 4px;

    padding: 4px 8px;
    color: #ffffff;
    background-color: ${({ theme }) => theme.gray6};

    opacity: 0;

    transition: opacity 200ms linear;
    pointer-events: none;
  }

  &:hover {
    &:after {
      opacity: 1;
    }
  }
`;

export const Icon = styled.img``;
