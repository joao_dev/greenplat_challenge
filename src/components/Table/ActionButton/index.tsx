import { ButtonHTMLAttributes, FC } from 'react';

import { Container, Icon } from './styles';

interface IActionButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  tooltip: string;
  iconPath: string;
  value?: string;
}

const ActionButton: FC<IActionButtonProps> = ({
  iconPath,
  onClick,
  tooltip,
  value = '',
  disabled = false,
}) => {
  return (
    <Container
      type="button"
      onClick={onClick}
      tooltip={tooltip}
      value={value}
      disabled={disabled}
    >
      <Icon loading="lazy" src={iconPath} />
    </Container>
  );
};

ActionButton.displayName = 'ActionButton';

export default ActionButton;
