/* eslint-disable react/jsx-key */
import { FC, useContext, useEffect } from 'react';

import { ThemeContext } from 'styled-components';

import {
  HiChevronLeft,
  HiChevronDoubleLeft,
  HiChevronRight,
  HiChevronDoubleRight,
} from 'react-icons/hi';
import { useTable, usePagination } from 'react-table';
import PulseLoader from 'src/components/PulseLoader';

import {
  Button,
  Container,
  Controls,
  Pagination,
  Span,
  TableHtml,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from './styles';

export interface FetchDataParams {
  pageIndex: number;
  pageSize: number;
}

interface MyTableProps {
  columns: any;
  data: { [key: string]: any }[];
  loading: boolean;
  onChangePage({ pageIndex, pageSize }: FetchDataParams): void;
  totalData: number;
}

const Table: FC<MyTableProps> = ({
  columns,
  data,
  loading,
  onChangePage,
  totalData,
}) => {
  const themeContext = useContext(ThemeContext);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    // Get the state from the instance
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
      manualPagination: true,
      pageCount: Math.ceil(totalData / 10),
    },
    usePagination
  );

  useEffect(() => {
    onChangePage({ pageIndex, pageSize });
  }, [onChangePage, pageIndex, pageSize]);

  return (
    <Container>
      <TableHtml {...getTableProps()}>
        <Thead>
          {headerGroups.map((headerGroup) => (
            <Tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <Th {...column.getHeaderProps()}>{column.render('Header')}</Th>
              ))}
            </Tr>
          ))}
        </Thead>
        <Tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row);
            return (
              <Tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <Td {...cell.getCellProps()}>{cell.render('Cell')}</Td>
                  );
                })}
              </Tr>
            );
          })}
          <Tr>
            {loading ? (
              // Use our custom loading state to show a loading indicator
              <Td colSpan={10000}>
                <PulseLoader size={10} color={themeContext.primary} />
              </Td>
            ) : (
              <Td colSpan={10000}>
                Mostrando de {pageSize * pageIndex + 1} até{' '}
                {pageSize * (pageIndex + 1)}. Total de {totalData} resultado(s)
              </Td>
            )}
          </Tr>
        </Tbody>
      </TableHtml>
      {/* 
        Pagination can be built however you'd like. 
        This is just a very basic UI implementation:
      */}
      <Pagination className="pagination">
        <Controls>
          <Button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            <HiChevronDoubleLeft />
          </Button>
          <Button onClick={() => previousPage()} disabled={!canPreviousPage}>
            <HiChevronLeft />
          </Button>
          <Button onClick={() => nextPage()} disabled={!canNextPage}>
            <HiChevronRight />
          </Button>
          <Button
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}
          >
            <HiChevronDoubleRight />
          </Button>
          <Span>
            Página {pageIndex + 1} de {pageOptions.length}
          </Span>
        </Controls>
      </Pagination>
    </Container>
  );
};

export default Table;
