import styled from 'styled-components';

import media from 'src/styles/configs/devices';

export const Container = styled.div`
  ${media.max.tablet} {
    display: block;
    max-width: 100%;
    overflow-x: scroll;
    overflow-y: hidden;
    border-bottom: 1px solid black;
  }
`;

export const TableHtml = styled.table`
  width: 100%;
  border-spacing: 0;
  border: 1px solid ${({ theme }) => theme.table.borderColor};
  border-radius: 2px;
`;

export const Thead = styled.thead`
  background-color: ${({ theme }) => theme.table.backgroundColorHeader};
`;

export const Tbody = styled.tbody`
  background-color: ${({ theme }) => theme.table.backgroundColorBody};

  tr:last-child {
    td {
      border-bottom: 0;
    }
  }
`;

export const Tr = styled.tr``;

export const Td = styled.td`
  font-size: 14px;
  font-weight: 400;
  color: ${({ theme }) => theme.gray4};

  margin: 0;
  padding: 8px;
  border-bottom: 1px solid ${({ theme }) => theme.table.borderColor};
  border-right: 1px solid ${({ theme }) => theme.table.borderColor};

  :last-child {
    border-right: 0;
  }
`;

export const Th = styled.td`
  font-size: 14px;
  font-weight: 600;
  color: ${({ theme }) => theme.gray6};

  text-transform: uppercase;
  margin: 0;
  padding: 10px;
  border-bottom: 1px solid ${({ theme }) => theme.table.borderColor};
  border-right: 1px solid ${({ theme }) => theme.table.borderColor};
  :last-child {
    border-right: 0;
  }
`;

export const Pagination = styled.div`
  padding: 16px 0;
`;

export const Controls = styled.div`
  display: flex;
  align-items: center;

  font-size: 14px;
`;

export const Button = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;

  border: none;
  background: none;

  margin-right: 8px;

  &:last-of-type {
    margin-right: 24px;
  }

  svg {
    color: ${({ theme }) => theme.gray5};

    font-size: 20px;
    pointer-events: none;

    transition: color 200ms linear;
  }

  &:not(:disabled):hover {
    svg {
      color: ${({ theme }) => theme.primary};
    }
  }

  &:disabled {
    svg {
      color: ${({ theme }) => theme.gray1};
    }
  }
`;

export const Span = styled.span``;
