import styled from 'styled-components';

import media from 'src/styles/configs/devices';

interface ContainerPropsInterface {
  columns: string;
  gap?: number;
}

export const Container = styled.div`
  display: grid;
  grid-gap: ${({ gap }: ContainerPropsInterface) => gap}px;
  width: 100%;

  ${media.tablet} {
    grid-template-columns: ${({ columns }: ContainerPropsInterface) => columns};
    grid-template-rows: 1fr;
  }
`;
