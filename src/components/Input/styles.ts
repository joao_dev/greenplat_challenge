import styled, { css } from 'styled-components';

export const Container = styled.div``;

export const Field = styled.input`
  width: 100%;
  border-radius: 4px;
  padding: 8px;
  margin-top: 4px;
  height: 38px;
  font-size: 14px;

  ${({ theme }) => css`
    color: ${theme.input.color}}
    border: 1px solid ${theme.input.borderColor}}
    background-color: ${theme.input.backgroundColor}}
  `}
`;
