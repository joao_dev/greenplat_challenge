import { FC, InputHTMLAttributes, useCallback, useRef } from 'react';

import masks from 'magic-masks';
import Label from 'src/components/Label';

import { Container, Field } from './styles';

interface IInputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
  mask?: string;
}

const Input: FC<IInputProps> = ({
  defaultValue,
  label,
  mask,
  name,
  required = false,
  type = 'text',
  max,
  min,
}) => {
  const inputRef = useRef<HTMLInputElement>(null);

  // Adding mask to value before set in input if mask is not empty
  const handleOnChange = useCallback(
    (event) => {
      if (mask && inputRef.current) {
        const { value } = event.target;

        const updatedValue = masks[mask](value);
        inputRef.current.value = updatedValue;
      }
    },
    [mask]
  );

  return (
    <Container>
      <Label htmlFor={name}>
        {label} {required ? '*' : ''}
      </Label>
      <Field
        ref={inputRef}
        type={type}
        defaultValue={defaultValue}
        name={name}
        onChange={handleOnChange}
        required={required}
        min={min}
        max={max}
      />
    </Container>
  );
};

export default Input;
