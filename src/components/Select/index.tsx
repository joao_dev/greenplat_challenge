import { createRef, FC, Ref, SelectHTMLAttributes } from 'react';

import { Props as SelectProps } from 'react-select';
import Label from 'src/components/Label';

import { Container, Field } from './styles';

interface IOption {
  label: string;
  value: string | number;
}

interface ISelectProps extends SelectHTMLAttributes<HTMLSelectElement> {
  label: string;
  options: IOption[];
  selectRef?: Ref<SelectProps>;
}

const Select: FC<ISelectProps> = ({
  label,
  name,
  required = false,
  onChange,
  options,
  selectRef = createRef(),
}) => {
  return (
    <Container>
      <Label htmlFor={name}>
        {label} {required ? '*' : ''}
      </Label>
      <Field
        ref={selectRef}
        name={name}
        required={required}
        onChange={onChange}
        options={options}
        noOptionsMessage={() => 'Sem opções'}
        placeholder="Selecione..."
        getValue={(value) => {
          console.log(value);
          return value;
        }}
      />
    </Container>
  );
};

export default Select;
