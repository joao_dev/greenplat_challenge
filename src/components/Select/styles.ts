import styled from 'styled-components';

import ReactSelect from 'react-select';

export const Container = styled.div``;

export const Field = styled(ReactSelect).attrs(({ theme }) => ({
  styles: {
    control: (provided, state) => ({
      ...provided,
      borderRadius: 4,
      paddingRight: 3,
      paddingLeft: 3,
      marginTop: 4,
      maxHeight: 30,
      fontSize: 14,
      border: `1px solid ${theme.input.borderColor}`,
      borderColor: state.isFocused
        ? `${theme.input.borderColor} !important`
        : 'inherit',
      backgroundColor: theme.input.backgroundColor,
      boxShadow: 'none',
    }),
  },
}))``;

export const Option = styled.option``;
