import { SetStateAction } from 'react';
import { createContext } from 'react';

export interface IUser {
  id: string;
  full_name: string;
  cpf: string;
  born_date: string;
  state: string;
  city: string;
}

export interface ICities {
  label: string;
  value: number;
  stateId: number;
}

export interface IStates {
  label: string;
  value: number;
}
export interface UserContextInterface {
  users: IUser[];
  userToEdit?: IUser;
  cities: ICities[];
  citiesOptions: ICities[];
  states: IStates[];
  changeState(value: number): void;
  addUser(value: IUser): void;
  cancelEdit(): void;
  deleteUser(id: string): void;
  editUser(user: IUser): void;
  updateUser(value: IUser): void;
}

export const UserContext = createContext<UserContextInterface | null>(null);

export const UserContextProvider = UserContext.Provider;

export const UserContextConsumer = UserContext.Consumer;
