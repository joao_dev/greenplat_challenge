import { FC } from 'react';

import Header from './Header';

const Public: FC = ({ children }) => {
  return (
    <>
      <Header />
      {children}
    </>
  );
};

export default Public;
