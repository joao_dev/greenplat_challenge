import { Link } from 'react-router-dom';

import { Container, Content, Logo } from './styles';
const Header = (): JSX.Element => {
  return (
    <Container>
      <Content>
        <Link to="/">
          <Logo src="/assets/logo.svg" />
        </Link>
      </Content>
    </Container>
  );
};

export default Header;
