import styled from 'styled-components';

export const Container = styled.div`
  background-color: ${({ theme }) => theme.header.backgroundColor};
`;

export const Content = styled.div`
  padding: 22px 20px;
  margin: 0 auto;
  max-width: 1366px;
`;

export const Logo = styled.img``;
