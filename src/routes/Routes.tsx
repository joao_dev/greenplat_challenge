import { FC } from 'react';

import { BrowserRouter, Switch } from 'react-router-dom';

import routes from '.';
import PublicRoute from './Public';

const { REACT_APP_TITLE_PREFIX } = process.env;

const Routes: FC = () => {
  const makeTitle = (title?: string, label?: string): string => {
    const pageTitleSuffix = title || label || '';
    const pageTitle = `${
      REACT_APP_TITLE_PREFIX || 'React APP'
    } - ${pageTitleSuffix}`;

    return pageTitle;
  };

  const makeRoute = (route) => {
    const pageTitle = makeTitle(route.title, route.label);

    return <PublicRoute title={pageTitle} {...route} />;
  };

  return (
    <BrowserRouter>
      <Switch>
        {routes.map(({ sub_routes, ...route }) => [
          makeRoute(route),
          sub_routes?.map((sub_route) => makeRoute(sub_route)),
        ])}
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
