import { FC } from 'react';

import Home from 'src/pages/Home';
import NotFound from 'src/pages/NotFound';

export interface SubRouteInterface extends RouteInterface {
  show_in_sub_menu?: boolean;
}

export interface RouteInterface {
  exact?: boolean;
  component: FC;
  label: string;
  path: string;
  title?: string;
  sub_routes?: SubRouteInterface[];
}

export const publicRoutes: RouteInterface[] = [
  {
    component: Home,
    label: 'Página inicial',
    exact: true,
    path: '/',
  },
  {
    component: NotFound,
    label: 'Página não encontrada',
    path: '*',
  },
];

export default publicRoutes;
